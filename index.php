<?php

// Import the Morse Library
include('morse-library.php');

// Display Header
    echo "
        <center>
        <h1>Morse Framework - Example</h1>
        <h2>The Morse code translation library</h2>
        </center>
    ";

// Convert text to Morse    
    $data = "morse data here";
    echo "Original Text: $data<br />";

    // The actual function
    morsetotext($data);

?>