<?php

// Morse to text
function morsetotext ($input) {

// More Lookup Table Alphabet
$input = str_replace("a", "•- ", "$input");
$input = str_replace("b", "-••• ", "$input");
$input = str_replace("c", "-•-• ", "$input");
$input = str_replace("d", "-•• ", "$input");
$input = str_replace("e", "• ", "$input");
$input = str_replace("f", "••-• ", "$input");
$input = str_replace("g", "--• ", "$input");
$input = str_replace("h", "•••• ", "$input");
$input = str_replace("i", "•• ", "$input");
$input = str_replace("j", "•--- ", "$input");
$input = str_replace("k", "-•- ", "$input");
$input = str_replace("l", "•-•• ", "$input");
$input = str_replace("m", "-- ", "$input");
$input = str_replace("n", "-• ", "$input");
$input = str_replace("o", "--- ", "$input");
$input = str_replace("p", "•--• ", "$input");
$input = str_replace("q", "--•- ", "$input");
$input = str_replace("r", "•-• ", "$input");
$input = str_replace("s", "••• ", "$input");
$input = str_replace("t", "- ", "$input");
$input = str_replace("u", "••- ", "$input");
$input = str_replace("v", "•••- ", "$input");
$input = str_replace("w", "•-- ", "$input");
$input = str_replace("x", "-••- ", "$input");
$input = str_replace("y", "-•-- ", "$input");
$input = str_replace("z", "--•• ", "$input");

// More Lookup Table Numbers
$input = str_replace("1", "•---- ", "$input");
$input = str_replace("2", "••--- ", "$input");
$input = str_replace("3", "•••-- ", "$input");
$input = str_replace("4", "••••- ", "$input");
$input = str_replace("5", "••••• ", "$input");
$input = str_replace("6", "-•••• ", "$input");
$input = str_replace("7", "--••• ", "$input");
$input = str_replace("8", "---•• ", "$input");
$input = str_replace("9", "----• ", "$input");
$input = str_replace("0", "----- ", "$input");

echo $input;

}

?>
